const fs = require('fs-extra');
const concat = require('concat');


(async function build() {
    const files = [
        './dist/a-client/runtime.js',
        './dist/a-client/polyfills.js',
        './dist/a-client/main.js'
    ]

    await fs.ensureDir('elements');
    await concat(files,'elements/dupa-component.js')
    
})()