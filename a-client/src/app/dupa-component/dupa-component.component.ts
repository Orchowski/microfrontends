import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dupa-component',
  templateUrl: './dupa-component.component.html',
  styleUrls: ['./dupa-component.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class DupaComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
