import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DupaComponentComponent } from './dupa-component.component';

describe('DupaComponentComponent', () => {
  let component: DupaComponentComponent;
  let fixture: ComponentFixture<DupaComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DupaComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DupaComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
