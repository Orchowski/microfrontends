import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements'
import { AppComponent } from './app.component';
import { DupaComponentComponent } from './dupa-component/dupa-component.component';

@NgModule({
  declarations: [
    AppComponent,
    DupaComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  entryComponents: [
    DupaComponentComponent
  ],
  providers: []
})
export class AppModule {
  constructor(private injector: Injector) { }
  ngDoBootstrap() {
    const el = createCustomElement(DupaComponentComponent, { injector: this.injector });
    customElements.define('dupa-component', el);
  }
}
