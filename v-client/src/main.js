import Vue from 'vue'
import App from './App.vue'
import Message from './Message.vue'

Vue.component('vue-message', Message);
new Vue({
  el: '#app',
  render: h => h(App)
})